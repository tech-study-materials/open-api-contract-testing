package sandbox.sampleapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenApiContractTestingApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenApiContractTestingApplication.class, args);
	}

}
