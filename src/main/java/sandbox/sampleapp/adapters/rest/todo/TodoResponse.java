package sandbox.sampleapp.adapters.rest.todo;

import lombok.Value;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.UUID;

@Value
class TodoResponse {
    @NotNull UUID id;
    @NotBlank String description;
    String avatarUrl;
    LocalDate dueDate;
}
