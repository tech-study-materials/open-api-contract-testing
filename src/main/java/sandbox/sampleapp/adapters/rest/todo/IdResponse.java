package sandbox.sampleapp.adapters.rest.todo;

import lombok.Value;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Value(staticConstructor = "of")
class IdResponse {
    @NotNull UUID id;
}
