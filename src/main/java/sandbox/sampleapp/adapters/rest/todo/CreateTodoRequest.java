package sandbox.sampleapp.adapters.rest.todo;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;

@Data
class CreateTodoRequest {
    @NotBlank String description;
    LocalDate dueDate;
}
