package sandbox.sampleapp.adapters.rest.todo;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

import static java.util.Collections.emptyList;

@RestController
@SecurityRequirement(name = "jwt")
class TodoController {

    @PostMapping("/user/{userId}/todo")
    IdResponse createTodo(
            @PathVariable String userId,
            @Valid @RequestBody CreateTodoRequest req) {
        return IdResponse.of(UUID.randomUUID());
    }

    @GetMapping("/user/{userId}/todo")
    List<TodoResponse> fetchAll(@PathVariable String userId) {
        return emptyList();
    }
}

