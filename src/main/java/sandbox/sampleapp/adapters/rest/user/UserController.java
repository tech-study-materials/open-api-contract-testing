package sandbox.sampleapp.adapters.rest.user;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
class UserController {

    @PostMapping("/registration")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @Operation(summary ="Account registration", description = "This endpoint will create a disabled user account, and email a verification link to the user.")
    @ApiResponses({
            @ApiResponse(responseCode = "202", description = "registration successful"),
            @ApiResponse(responseCode = "409", description = "username (email) already exists"),
            @ApiResponse(responseCode = "400", description = "invalid or missing registration data")
    })
    void register(@Valid @RequestBody RegistrationForm registrationForm) {}

    @PostMapping("/registration/{registrationId}/verification")
    @Operation(summary = "Account verification", description = "This endpoint will verify and enable previously registered account")
    VerificationResponse verify(@PathVariable UUID registrationId) {
        return VerificationResponse.builder()
                .email("someone@somewhere.com")
                .userId(UUID.randomUUID())
                .build();
    }
}