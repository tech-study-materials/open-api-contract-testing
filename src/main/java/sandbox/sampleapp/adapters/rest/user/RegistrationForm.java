package sandbox.sampleapp.adapters.rest.user;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
class RegistrationForm {
    @NotBlank @Email String email;
    @NotBlank String password;
}
