package sandbox.sampleapp.adapters.rest.user;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
class VerificationResponse {
    String email;
    UUID userId;
}
