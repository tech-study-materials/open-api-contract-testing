package sandbox.sampleapp;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.StreamSupport.stream;

@RequiredArgsConstructor
public class JsonComparator {

    /**
     * @return
     *      empty list if inputs are semantically equal
     *      or otherwise a diff in human-readable format
     *          (list of actions to apply to contractAsYaml to make it equal to currentApiAsYaml)
     */
    public static List<String> compare(String currentApiAsYaml, String contractAsYaml) throws IOException {
        ObjectMapper jackson = new ObjectMapper(new YAMLFactory());

        var diff = JsonDiff.asJson(
                jackson.readTree(new StringReader(contractAsYaml)),
                jackson.readTree(new StringReader(currentApiAsYaml)));

        return streamOf(diff::elements)
                .map(JsonComparator::toYaml)
                .toList();
    }

    private static Stream<JsonNode> streamOf(Iterable<JsonNode> elements) {
        return stream(elements.spliterator(), false);
    }

    @SneakyThrows
    private static String toYaml(JsonNode jsonNode) {
        return new YAMLMapper().writeValueAsString(jsonNode);
    }

    // how does it work : -------------------------------------------
    public static void main(String[] args) throws IOException {
        compare(yaml_1, yaml_2).forEach(System.out::println);
    }

    static final String yaml_1 =
"""
email:
    from: Adam
    to:
        - Joe
        - Tom
        - Kim
    subject: "greetings"
    message: "Hello everyone, hope you have a good day!"
""";
    static final String yaml_2 =
"""
email:
    from: Adam
    to:
        - Joe
        - Ben
        - Kim
    message: "Hello everyone, hope you don't have a good day!"
    signature: xxx
""";
}
