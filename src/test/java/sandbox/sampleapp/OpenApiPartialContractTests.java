package sandbox.sampleapp;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.parser.OpenAPIV3Parser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.Files.contentOf;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static sandbox.sampleapp.JsonComparator.compare;

@SpringBootTest(
		webEnvironment = RANDOM_PORT,
		properties = {
				"springdoc.group-configs[0].group=user",
				"springdoc.group-configs[0].packagesToScan=sandbox.sampleapp.adapters.rest.user",
				"springdoc.group-configs[1].group=todo",
				"springdoc.group-configs[1].packagesToScan=sandbox.sampleapp.adapters.rest.todo",
		}
)
@Slf4j
class OpenApiPartialContractTests {

	@Autowired TestRestTemplate rest;

	@ParameterizedTest
	@ValueSource(strings = {"user","todo"})
	void contractShouldBeFulfilled(String contractPart) throws IOException {

		String currentApi = sanitize(rest.getForObject("/v3/api-docs/" + contractPart, String.class));
		//dump to a file, so it can be inspected | compared manually | copied into contract | whatever
		Files.writeString(Path.of("build/"+contractPart+".openapi.current.yml"), currentApi);

		String contract = toYaml(parse(contentOf(new File("contracts/"+contractPart+".openapi.yml"), UTF_8.name())));

		var diffs = compare(currentApi, contract);
		assertThat(diffs)
				.withFailMessage("""
						Contract.vs.currentAPI - %d difference(s) found.
						To fix, apply the following changes to the '%s' contract:
						%s
						""" , diffs.size(), contractPart, String.join("", diffs))
				.isEmpty();
	}

	private String sanitize(String jsonOrYaml) throws JsonProcessingException {
		OpenAPI openAPI = parse(jsonOrYaml);
		removeIgnoredElements(openAPI);
		return toYaml(openAPI);
	}

	private String toYaml(OpenAPI openAPI) throws JsonProcessingException {
		return Yaml.mapper().writeValueAsString(openAPI);
	}

	private OpenAPI parse(String jsonOrYaml) {
		var parsed = new OpenAPIV3Parser().readContents(jsonOrYaml);
		assertThat(parsed.getMessages()).isEmpty();
		return parsed.getOpenAPI();
	}

	private void removeIgnoredElements(OpenAPI openAPI) {
		//servers are always different, springdoc adds localhost of currently running server (random port)
		//Let's set it to fixed value, so that it can be used for local testing in swaggerUI
		openAPI.servers(List.of(new Server().url("http://localhost:8080")));
	}
}
