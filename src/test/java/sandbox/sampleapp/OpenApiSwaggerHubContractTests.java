package sandbox.sampleapp;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.core.util.Yaml;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.servers.Server;
import io.swagger.v3.parser.OpenAPIV3Parser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static sandbox.sampleapp.JsonComparator.compare;

@SpringBootTest(
		webEnvironment = RANDOM_PORT,
		properties = "springdoc.packagesToScan=sandbox.sampleapp.adapters.rest"
)
@Slf4j
class OpenApiSwaggerHubContractTests {

	static final String OPENAPI_CURRENT_YML = "build/all.openapi.current.yml";
	@Autowired TestRestTemplate rest;

	@Test
	void contractShouldBeFulfilled() throws Exception {

		var currentApi = sanitize(rest.getForObject("/v3/api-docs", String.class));
		//dump to a file, so it can be inspected | compared manually | copied into contract | whatever
		Files.writeString(Path.of(OPENAPI_CURRENT_YML), toYaml(currentApi));

		var contract = parse(getSwaggerHubVersion(currentApi.getInfo().getVersion()));

		var diffs = compare(
				toYaml(currentApi),
				toYaml(contract));
		assertThat(diffs)
				.withFailMessage("""
						Contract.vs.currentAPI - %d difference(s) found.
						To fix, apply the following changes to the contract:
						%s
						""" , diffs.size(), String.join("", diffs))
				.isEmpty();
	}

	private String toYaml(OpenAPI currentApi) throws JsonProcessingException {
		return Yaml.mapper().writeValueAsString(currentApi);
	}

	private OpenAPI sanitize(String jsonOrYaml) {
		OpenAPI openAPI = parse(jsonOrYaml);
		removeIgnoredElements(openAPI);
		return openAPI;
	}

	private OpenAPI parse(String jsonOrYaml) {
		var parsed = new OpenAPIV3Parser().readContents(jsonOrYaml);
		assertThat(parsed.getMessages()).isEmpty();
		return parsed.getOpenAPI();
	}

	private void removeIgnoredElements(OpenAPI openAPI) {
		//servers are always different, springdoc adds localhost of currently running server (random port)
		openAPI.servers(List.of(new Server().url("http://localhost:8080")));
	}

	static final String SWAGGERHUB_API_LOCATION = "https://api.swaggerhub.com/apis/szczebel/open-api-contract-testing/";
	String getSwaggerHubVersion(String version) throws URISyntaxException {
		try {
			return new RestTemplate()
					.getForObject(new URI(SWAGGERHUB_API_LOCATION + version), String.class);
		} catch (Exception e) {
			if(e.getMessage().contains("Unknown API"))
				fail("API not found under %s. Version changed?\n" +
						"Anyways, upload your version, you can find it here: %s",
						SWAGGERHUB_API_LOCATION + version, OPENAPI_CURRENT_YML);
			throw e;
		}
	}
}
